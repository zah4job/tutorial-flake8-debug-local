# Tutorial to show how debug flake8 plugins locally

You will neeed

- python3.10
- poetry module installed

Just do poetry install and run 

> flake8 .

in project folder.
