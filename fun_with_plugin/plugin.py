import ast
from typing import Generator, Tuple, Type, Any

run_return = Generator[Tuple[int, int, str, Type[Any]], None, None]


def is_class(element: ast.stmt, class_name: str) -> bool:
    return isinstance(element, ast.ClassDef) and element.name == class_name


def is_based(element: ast.ClassDef, base_name: str) -> bool:
    for base in element.bases:
        if hasattr(base, 'attr'):
            if base.attr == base_name:
                return True
    return False


def has_nested_class(element: ast.ClassDef, class_name: str) -> bool:
    for body_element in element.body:
        if is_class(body_element, class_name):
            return True
    return False


class Plugin(object):
    name = 'brains_shield'
    version = 100

    def __init__(self, tree: ast.AST, *args) -> None:
        self._tree = tree

    def _is_class(self, element):
        return isinstance(element, ast.ClassDef)

    def run(self) -> run_return:
        for element in self._tree.body:
            if element is None:
                continue
            if self._is_class(element) and is_based(element, 'Model'):
                if not has_nested_class(element, 'Helmet'):
                    message = 'HMT001 Не хватает защиты '
                    message += 'добавьте вложенный класс Helmet'
                    yield element.lineno, 0, message, type(self)
