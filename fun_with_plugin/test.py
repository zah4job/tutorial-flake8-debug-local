from django.db import models


class JustMozg(models.Model):
    iq = models.PositiveSmallIntegerField(default=30)

    class Meta(object):
        material = 'chugun'


class ProtectedBrain(models.Model):
    iq = models.PositiveSmallIntegerField(default=149)


class Human(object):
    is_brain = False
